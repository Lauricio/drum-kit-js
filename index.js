"use strict"


const sons = {
    "A": "boom.wav",
    "S": "clap.wav",
    "D": "hihat.wav",
    "F":"kick.wav",
    "G": "openhat.wav",
    "H": "ride.wav",
    "J": "snare.wav",
    "K": "tink.wav",
    "L": "tom.wav"
}

const criarDiv = (texto) => {
    const div = document.createElement("div");
    div.classList.add("key");
    div.textContent = texto;
    div.id = texto;
   document.getElementById("container").appendChild(div);

}

const exibir = (sons) => Object.keys(sons).forEach(criarDiv);

const tocarSon = (letra) => {
const audio = new Audio(`./sounds/${sons[letra]}`);
audio.play();
}

document.getElementById("container").addEventListener("click", function(evento){
    const letra = evento.target.id;
    const letrapermitida = sons.hasOwnProperty(letra);
     if(letrapermitida){
        tocarSon(letra);}
})

exibir(sons);
